<?php 
	function no_width_height( $html ) {
        // Remove width="" and height="" properties from the img tag
        return preg_replace( '/(height|width)="\d*"\s/', "", $html );
    }
 ?>