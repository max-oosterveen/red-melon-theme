<?php 
    function nav_classes_for_custom_post_types($classes, $item) {

        // Getting the current post details
        global $post;
        
        // Getting the post type of the current post
        $current_post_type = get_post_type_object(get_post_type($post->ID));
        $current_post_type_slug = $current_post_type->rewrite['slug'];
        
        if($current_post_type->name == "post"){
            $current_post_type_slug = get_post_field( 'post_name', get_option( 'page_for_posts' ) );
        }
        
        // Getting the URL of the menu item
        $menu_slug = strtolower(trim($item->url));            
        
        // If the menu item URL contains the current post types slug add the current-menu-item class
        if (strpos($menu_slug,$current_post_type_slug) !== false) {
           $classes[] = 'current-menu-parent';
        }
        
        // Return the corrected set of classes to be added to the menu item
        return $classes;
        
    }

    // add_action('nav_menu_css_class', 'nav_classes_for_custom_post_types', 10, 2 );
?>
