<?php
    if(class_exists('Walker_Nav_Menu')){
        class Main_Walker extends Walker_Nav_Menu {
            function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
                $classes = implode(" ", $item->classes);
                $output .= "<li class='{$classes}'>";
                if($item->url && $item->url != '#') {
                    $output .= "<a href='{$item->url}'>{$item->title}</a>";
                } else {
                    $output .= "<span>{$item->title}</span>";
                }
                if(in_array('menu-item-has-children', $item->classes)){
                    $output .= "<div class='sub-menu'>";
                        $output .= "<div class='sub-menu--wrapper'>";
                            $output .= "<div class='sub-menu--wrapper_menu bg blue dark'>";
                                $output .= "<div class='sub-menu--wrapper_menu--inner'>";
                                    $output .= "<button class='go-back'><span class='icon'><svg width='24' height='24' xmlns='http://www.w3.org/2000/svg' fill-rule='evenodd' clip-rule='evenodd'><path fill='#fff' d='M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z'/></svg></span>Terug</button>";
                                    $output .= "<span class='h3'><a href='{$item->url}'>{$item->title}</a></span>";
                }
            }

            function end_el(&$output, $item, $depth = 0, $args = array()){
                if(in_array('menu-item-has-children', $item->classes)){
                                $output .= "</div>";
                            $output .= "</div>";
                            $output .= "<div class='sub-menu--wrapper_content'>";
                                $output .= "<figure class='inline-cover'>".no_width_height(wp_get_attachment_image(get_field('big_menu_image', $item->ID), 'full'))."</figure>";
                            $output .= "</div>";
                        $output .= "</div>";
                    $output .= "</div>";
                }

                $output .= "</li>";
            }
        }
    }
?>