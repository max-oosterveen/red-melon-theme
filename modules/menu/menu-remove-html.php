<?php 
    function remove_menu_container($args = ''){
        $args['container'] = false;
        return $args;
    }
    add_filter('wp_nav_menu_args', 'remove_menu_container');

    function remove_menu_ul($menu){
        return preg_replace(array('#^<ul[^>]*>#', '#</ul>$#' ), '', $menu);
    }
    add_filter('wp_nav_menu', 'remove_menu_ul');
?>
