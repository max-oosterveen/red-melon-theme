<?php 
    // snippets
    require "setup/init.php";

    // snippets
    require "snippets/init.php";
  
    // admin
    require "admin/init.php";

    // Menu
    require "menu/init.php";

  
  
    function register_styles_and_scripts() {
        wp_enqueue_style(
            'custom-style', 
            get_template_directory_uri() . '/public/css/style.min.css',
            '1.0.0'
        );

        wp_enqueue_script(
            'all', 
            get_template_directory_uri() . '/public/js/all.min.js',
            array(),
            false,
            true
        );

        wp_enqueue_script(
            'jquery'
        );

        wp_localize_script(
            'custom-js', 
            'custom_ajax', 
            array('ajax_url' => admin_url('admin-ajax.php'))
        );


    }
    
    add_action( 'wp_enqueue_scripts', 'register_styles_and_scripts', 10 );


    function deregister_styles_and_scripts(){
        wp_dequeue_style('wp-block-library');
    }

    add_action('wp_print_styles', 'deregister_styles_and_scripts');
 ?>