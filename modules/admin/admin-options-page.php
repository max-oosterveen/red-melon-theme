<?php
    if(function_exists('acf_add_options_page')) {    
        // Add options page for general site options 
        acf_add_options_page(__('Algemene opties', 'Privatescan'));
        
        acf_add_options_page(__('Statische blokken', 'Privatescan'));
    }
?>