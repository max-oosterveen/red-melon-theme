<?php
    function change_yoast_priority() {
        // Set priority of Yoast metabox to low so it shows at the bottom of the page
        return 'low';
    }
    add_filter( 'wpseo_metabox_prio', 'change_yoast_priority');
?>