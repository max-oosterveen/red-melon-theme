<?php 
	function remove_admin_menu_items() {
        remove_menu_page( 'edit-comments.php' );     
        remove_menu_page( 'edit.php' );  
    }
    add_action( 'admin_menu', 'remove_admin_menu_items' );
 ?>