<?php 
    // login
    require "admin-login.php";

    // menu
    require "admin-menu.php";

    // ACF options page
    require "admin-options-page.php";

    // Yoast position
    require "admin-yoast-position.php";

    function admin_enqueue(){
        wp_enqueue_style(
            'admin-style', 
            get_template_directory_uri() . '/assets/css/admin.css',
            '1.0.0'
        );
    }

    add_action('admin_enqueue_scripts', 'admin_enqueue');
 ?>