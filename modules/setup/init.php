<?php 
    // Custom post types 
    require "setup-custom-posts.php";

    // Custom taxonomies
    require "setup-custom-taxonomies.php";

    // Allow SVG Upload
    require "setup-allow-svg-upload.php";

    // Theme support
    require "setup-theme-support.php";

    // Theme support
    require "setup-remove-emoij.php";
  
 ?>