<?php 
    function create_custom_taxonomies() {
        $default_args = array(
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => false,
        );

        $custom_taxonomies = array(
            array(
                'taxonomy'      => 'faq_subject',
                'name'          => 'FAQ onderwerpen',
                'singular_name' => 'FAQ onderwerp',
                'plural_name'   => 'FAQ onderwerpen',
                'slug'          => 'subject',
                'post_type'     => array('faq'),
                'settings'      => $default_args,
            ),
            array(
                'taxonomy'      => 'knowledge-base_subject',
                'name'          => 'Kennisbank onderwerpen',
                'singular_name' => 'Kennisbank onderwerp',
                'plural_name'   => 'Kennisbank onderwerpen',
                'slug'          => 'subject',
                'post_type'     => array('knowledge-base'),
                'settings'      => $default_args,
            ),
        );

        foreach($custom_taxonomies as $custom_taxonomy){
            $labels = array(
                'name'                       => _x($custom_taxonomy['name'], 'Post Type General Name', 'Privatescan' ),
                'singular_name'              => _x($custom_taxonomy['singular_name'], 'Post Type Singular Name', 'Privatescan' ),
                'menu_name'                  => __($custom_taxonomy['name'], 'Privatescan' ),
                'all_items'                  => __($custom_taxonomy['plural_name'], 'Privatescan'),
                'parent_item'                => __('Parent Item', 'Privatescan'),
                'parent_item_colon'          => __('Parent Item:', 'Privatescan'),
                'new_item_name'              => __('New Item Name', 'Privatescan'),
                'add_new_item'               => __($custom_taxonomy['singular_name'] . ' toevoegen', 'Privatescan' ),
                'edit_item'                  => __($custom_taxonomy['singular_name'] . ' bewerken', 'Privatescan' ),
                'update_item'                => __($custom_taxonomy['singular_name'] . ' bijwerken', 'Privatescan' ),
                'view_item'                  => __($custom_taxonomy['singular_name'] . ' bekijken', 'Privatescan' ),
                'separate_items_with_commas' => __('Separate items with commas', 'Privatescan'),
                'add_or_remove_items'        => __('Add or remove items', 'Privatescan'),
                'choose_from_most_used'      => __('Choose from the most used', 'Privatescan'),
                'popular_items'              => __('Popular Items', 'Privatescan'),
                'search_items'               => __('Search Items', 'Privatescan'),
                'not_found'                  => __('Niet gevonden', 'Privatescan' ),
                'no_terms'                   => __('No items', 'Privatescan'),
                'items_list'                 => __($custom_taxonomy['name'], 'Privatescan' ),
                'items_list_navigation'      => __('Items list navigation', 'Privatescan'),
            );
            $args = array(
                'labels'                     => $labels,
            );

            $args = array_merge($args, $custom_taxonomy['settings']);

            register_taxonomy($custom_taxonomy['taxonomy'], $custom_taxonomy['post_type'], $args);
        }

    }
    add_action('init', 'create_custom_taxonomies', 0);
 ?>