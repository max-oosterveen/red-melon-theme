<?php 
    function create_post_types(){
        $default_args = array(
            'supports'              => array('title'),
            'taxonomies'            => array(),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
        );

        $custom_post_types = array(
            array(
                'post_type'     => 'news',
                'name'          => 'Nieuws',
                'singular_name' => 'Nieuwsbericht',
                'plural_name'   => 'Nieuwsberichten',
                'slug'          => 'nieuws',
                'settings'      => $default_args,
            ),
        );

        foreach($custom_post_types as $custom_post_type){
            $labels = array(
                'name'                  => _x($custom_post_type['name'], 'Post Type General Name', 'Privatescan' ),
                'singular_name'         => _x($custom_post_type['singular_name'], 'Post Type Singular Name', 'Privatescan' ),
                'menu_name'             => __($custom_post_type['name'], 'Privatescan' ),
                'name_admin_bar'        => __($custom_post_type['name'], 'Privatescan' ),
                'archives'              => __($custom_post_type['name'], 'Privatescan' ),
                'attributes'            => __('Attributen', 'Privatescan' ),
                'parent_item_colon'     => __('Parent:', 'Privatescan' ),
                'all_items'             => __($custom_post_type['plural_name'], 'Privatescan' ),
                'add_new_item'          => __($custom_post_type['singular_name'] . ' toevoegen', 'Privatescan' ),
                'add_new'               => __($custom_post_type['singular_name'] . ' toevoegen', 'Privatescan' ),
                'new_item'              => __('Nieuw nieuwsbericht', 'Privatescan' ),
                'edit_item'             => __($custom_post_type['singular_name'] . ' bewerken', 'Privatescan' ),
                'update_item'           => __($custom_post_type['singular_name'] . ' bijwerken', 'Privatescan' ),
                'view_item'             => __($custom_post_type['singular_name'] . ' bekijken', 'Privatescan' ),
                'view_items'            => __($custom_post_type['plural_name'] . ' bekijken', 'Privatescan' ),
                'search_items'          => __($custom_post_type['plural_name'] . ' zoeken', 'Privatescan' ),
                'not_found'             => __('Niet gevonden', 'Privatescan' ),
                'not_found_in_trash'    => __('Niet gevonden in de prullenbak', 'Privatescan' ),
                'featured_image'        => __('Uitgelichte afbeelding', 'Privatescan' ),
                'set_featured_image'    => __('Uitgelichte afbeelding instellen', 'Privatescan' ),
                'remove_featured_image' => __('Uitgelichte afbeelding verwijderen', 'Privatescan' ),
                'use_featured_image'    => __('Als uitgelichte afbeelding gebruiken', 'Privatescan' ),
                'insert_into_item'      => __('Insert into item', 'Privatescan' ),
                'uploaded_to_this_item' => __('Uploaded to this item', 'Privatescan' ),
                'items_list'            => __($custom_post_type['name'], 'Privatescan' ),
                'items_list_navigation' => __('Items list navigation', 'Privatescan' ),
                'filter_items_list'     => __('Filter items list', 'Privatescan' ),
            );

            $args = array(
                'label'     => __($custom_post_type['plural_name'], 'Privatescan' ),
                'labels'    => $labels,
                'rewrite'   => array(
                    'slug'  => $custom_post_type['slug'],
                ),
            );

            $args = array_merge($args, $custom_post_type['settings']);

            register_post_type($custom_post_type['post_type'], $args);

            if($default_args['has_archive'] == true && function_exists('acf_add_options_sub_page')){
                acf_add_options_sub_page(array(
                    'page_title'    => __($custom_post_type['name'] . ' overzicht opties', 'Privatescan'),
                    'menu_title'    => __($custom_post_type['name'] . ' overzicht opties', 'Privatescan'),
                    'parent_slug'   => 'edit.php?post_type=' . $custom_post_type['post_type'],
                    'redirect'		=> false
                ));
            }
        }
    }

    add_action('init', 'create_post_types', 1);
 ?>