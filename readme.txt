=== Red Melon Theme ===

WordPress theme by Red Melon

== Description ==

Test is a WooCommerce compatible theme that works great for any kind of shop. Test features color and font options, blog options, integration with the Page Builder plugin so you can easily build your homepage or other pages and many more other useful features.

== Frequently Asked Questions ==

== Changelog ==

= 1.0.0 - Feb 01 2020 =
* Initial release