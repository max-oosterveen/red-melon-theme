const gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    minify = require("gulp-babel-minify");


function scripts(){
    return gulp
        .src('./assets/js/*.js')
        .pipe(concat('all.js'))
        .pipe(minify({
            mangle: {
              keepClassName: true
            }
          }))
        .pipe(rename('all.min.js'))
        .pipe(gulp.dest('./dist/js'));
}

function styles(){
    return gulp
        .src('./assets/scss/style.scss')
        .pipe(plumber())
        .pipe(sass({
            outputStyle: 'compressed',
            errLogToConsole: true
        }))
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('./dist/css'));
}

exports.watch = function() {
    gulp.watch('assets/scss/*.scss', styles);
    gulp.watch('assets/scss/*/*.scss', styles);

    gulp.watch('assets/js/*.js', scripts);
};
