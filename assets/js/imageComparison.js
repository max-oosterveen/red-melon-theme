const imageComparison = document.querySelectorAll('.image-comparison');

imageComparison.forEach((el) => {
    let comparisonHandler = el.querySelector('.image-comparison--handler');
    let resizeableImage = el.querySelector('.resizeable-image');
    let imageComparisonContainer = el.getBoundingClientRect().width;

    const images = el.querySelectorAll('img');
    images.forEach((image) => {
        image.setAttribute('draggable', false);
    })

    comparisonHandler.addEventListener('touchstart', imageComparison, true);
    comparisonHandler.addEventListener('mousedown', imageComparison, true);
        
    function imageComparison(){

        window.addEventListener('mousemove', startCompare, false);
        window.addEventListener('touchmove', startCompare, false);
        window.addEventListener('touchend', stopCompare, false);
        window.addEventListener('mouseup', stopCompare, false);

        function startCompare(e){
            if(e.type == 'touchmove'){
                var positionLeft = e.changedTouches[0].pageX - resizeableImage.getBoundingClientRect().left;
            } else {
                var positionLeft = e.pageX - resizeableImage.getBoundingClientRect().left;
            }


            if(positionLeft > 0 && positionLeft < imageComparisonContainer){
                resizeableImage.style.width = ((positionLeft / imageComparisonContainer) * 100) + '%';
                comparisonHandler.style.left = ((positionLeft / imageComparisonContainer) * 100) + '%';
            }
        }

        function stopCompare(){
            window.removeEventListener('touchmove', startCompare, false);
            window.removeEventListener('mousemove', startCompare, false);
        }
    }
})