async function fetchData() {
    try {
        [pages, projects] = await Promise.all([
            fetch('/wp-json/pages/get').then((response) => response.json()),
            fetch('/wp-json/projects/get').then((response) => response.json())
        ]);
    } catch (error){

    }
}

const results = document.querySelector('.search--results_lists'),
    pagesList  = results.querySelector('.search--results_list .pages'),
    projectsList  = results.querySelector('.search--results_list .projects'),
    searchResultsPreview = document.querySelector('.search--results_preview'),
    search_input = document.getElementById('s');

let search_term = '';

const showResults = async () => {

    // getting the data
    await fetchData();

    pagesList.innerHTML = '';
    projectsList.innerHTML = '';

    if(search_term){
        pages
            .filter(page =>
                page.title.toLowerCase().includes(search_term.toLowerCase())
            )
            .forEach(page => {
                const pageItem = document.createElement('li'),
                    pageItemLink = document.createElement('a'),
                    pageName = document.createElement('span'),
                    pageItemLink = document.createElement('a'),
                    pageName = document.createElement('span');
                    
                pageItem.classList.add('pages-item');


                pageItemLink.href = page.link;
                pageName.innerText = page.title;

                pageItemLink.appendChild(pageName);
                pageItem.appendChild(pageItemLink);

                pagesList.appendChild(pageItem);
                
                pageItem.addEventListener('mouseenter', () => {
                    searchResultsPreview.innerHTML = '';

                    const previewAuthor = document.createElement('span'),
                        previewTitle = document.createElement('span');
                        previewThumbnail = document.createElement('img');
                        
                    previewAuthor.innerText = page.author;
                    previewTitle.innerText = page.title;
                    previewThumbnail.src = page.thumbnail;

                    searchResultsPreview.appendChild(previewThumbnail);
                    searchResultsPreview.appendChild(previewTitle);
                    searchResultsPreview.appendChild(previewAuthor);
                })
        });
        projects
            .filter(project =>
                project.title.toLowerCase().includes(search_term.toLowerCase())
            )
            .forEach(project => {

                const projectItem = document.createElement('li'),
                    projectItemLink = document.createElement('a'),
                    projectName = document.createElement('span');

                projectItem.classList.add('pages-item');
                
                projectItemLink.href = project.link;
                projectName.innerText = project.title;

                projectItemLink.appendChild(projectName);
                projectItem.appendChild(projectItemLink);

                projectsList.appendChild(projectItem);

                projectItem.addEventListener('mouseenter', () => {
                    searchResultsPreview.innerHTML = '';

                    const previewAuthor = document.createElement('span'),
                        previewTitle = document.createElement('span');
                        previewThumbnail = document.createElement('img');
                        
                    previewAuthor.innerText = project.author;
                    previewTitle.innerText = project.title;
                    previewThumbnail.src = project.thumbnail;

                    searchResultsPreview.appendChild(previewThumbnail);
                    searchResultsPreview.appendChild(previewTitle);
                    searchResultsPreview.appendChild(previewAuthor);
                })
        });

        // results.appendChild(pagesList);
        // results.appendChild(projectsList);
    }

}

search_input.addEventListener('input', e => {
    // saving the input value
    search_term = e.target.value;

    showResults();
});


// showResults();
