function inlineCoverImages(){
    // Get all inline cover elements
    const images = document.querySelectorAll('.inline-cover>img');

    // Loop over all the inline cover elements
    images.forEach((image) => {
        // Remove classes, needed for resize (I suppose?)
        image.classList.remove('vertical', 'horizontal');

        // Get parent item (.inline cover)
        let parent = image.parentElement;
        
        // Calculate image and parent ratio
        let imageRatio = image.offsetWidth / image.offsetHeight;
        let parentRatio = parent.offsetWidth / parent.offsetHeight;

        if(parentRatio > imageRatio){
            image.classList.add('vertical');
        } else {
            image.classList.add('horizontal');
        }

        // Show image
        image.style.visibility = 'visible';
    })
}

(function(){
    inlineCoverImages();
})();

window.almComplete = function(alm){
    inlineCoverImages();
 };