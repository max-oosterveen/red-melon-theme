/*jslint browser: true*/
/*global jQuery, console, window, document*/
// STICKY FOOTER DYNAMIC HEIGHT //
jQuery(document).ready(function () {
    "use strict";
	jQuery(".menu-item-has-children>a").each(function () {
		jQuery(this).after("<span class='pull'></span>");
	});
	
	jQuery("#top .pull").on("click", function (e) {
        e.preventDefault();
        jQuery(this).toggleClass("open");
        jQuery(this).parent().toggleClass("open");
        jQuery(this).next().slideToggle();
	});
    
    jQuery("input, textarea, select").on("input, change", function () {
        if (jQuery(this).val()) {
            jQuery(this).addClass("hasVal");
        } else {
            jQuery(this).removeClass("hasVal");
        }
    });
    
    jQuery("input, textarea, select").each(function () {
        if (jQuery(this).val()) {
            jQuery(this).addClass("hasVal");
        }
        if (jQuery(this).attr('placeholder')) {
            jQuery(this).addClass("hasPlaceholder hasVal");
        }
    });

    jQuery(".wpcf7-form-control-wrap, .woocommerce-input-wrapper").each(function () {
        jQuery(this).next().appendTo(jQuery(this));
    });
    
    jQuery("select + noscript + label").each(function () {
        jQuery(this).after(jQuery(this).prev());
    })
});


jQuery('.leaf').hover(
    function(){ 
        var animationTime = jQuery(this).css('animation');
        
        jQuery(this).addClass('animate'); 
    
    }
)



jQuery(window).scroll(function () {
    "use strict";
	var top = jQuery("#top"),
        scrolled = jQuery(document).scrollTop();
	if (scrolled > 0) {
		top.addClass("scrolled");
	} else {
		top.removeClass("scrolled");
	}
	
});

jQuery(function(){
    if(jQuery(document).scrollTop() > 0){
        jQuery("#top").addClass('scrolled');
    }
})

jQuery(window).resize(function () {
    "use strict";
    jQuery("#top ul").removeAttr("style");
});
